                              MEMBER

  INCLUDE('ctWindowHelper.inc'),once
  INCLUDE('svapi.inc'),ONCE

                              MAP
                                INCLUDE('svapifnc.inc'),once
                              END

ctWindowHelper.OnTop          PROCEDURE(BOOL pOnTop)
  CODE
  SetWindowPos( |
      0{PROP:Handle}, | 
      CHOOSE(~pOnTop, HWND_NOTOPMOST, HWND_TOPMOST), | 
      0, 0, 0, 0, | !These will be ignored
      SWP_NOMOVE + SWP_NOSIZE)
  