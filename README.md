# README #

This is a little quiz suggested by Mark Goldberg.  It provides a ctWindowHelper class that lets a window toggle its "OnTop" state.  IOW, it forces the window to stay on top when ON.  There is an example program called "OnTop", with sln, cwproj and clw.  There's also the ctWindowHelper.inc+clw.