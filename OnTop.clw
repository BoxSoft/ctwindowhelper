                              PROGRAM

  INCLUDE('ctWindowHelper.inc'),once

                              MAP
                                MyProc()
                              END

oWindowHelper                 ctWindowHelper
  CODE
  START(MyProc)

MyProc                        PROCEDURE()
UI:IsOnTop                      BOOL(FALSE)
MyWindow                        WINDOW('Test Window')
                                  CHECK('Is On Top'),USE(UI:IsOnTop),AT(10,10)
                                END
  CODE
  OPEN(MyWindow)
  ACCEPT
    CASE ACCEPTED()
    OF ?UI:IsOnTop;  MESSAGE('UI:IsOnTop['& UI:IsOnTop &']') 
      oWindowHelper.OnTop( UI:IsOnTop )
    END
  END
  